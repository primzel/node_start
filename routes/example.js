/**
 * Created by iqvis on 2/16/2015.
 */
var express=require("express");
var http=require("http").Server(express);
var io=require("socket.io")(http);

//var db=mongoose.connection;
//
//db.on("error",console.error);
//
//db.once('open',function(){
//    console.log("we are successfully connected with mongodb");
//});


var router=express.Router();

router.get('/',function(req,res){

    res.send("response from example controller");

});
router.get('/index',function(req,res){

    //db.connect("mongodb://localhost:27017/nvolv3");

    var model_content="jade is an amazing template engine with strong focus on preformance and other powerfull features";

    res.render("example/index",{title:"Hello World",content:model_content});
});


io.on("connection",function(socket){
    console.log("new user has been connected to us");

    socket.on("send",function(data){
        console.log("message is : "+data);
        socket.broadcast.emit("broadcast_message",data);
    });



    socket.broadcast.emit("on_new_connect","test data");

});

http.listen(3001, function(){
    console.log('listening on *:3000');
});



module.exports=router;



